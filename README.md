clamav
===

A container image that runs ClamAV and clamav-milter, with
additional "unofficial" signatures from sanesecurity and
other sources, self-updating.

Define CLAMD_PORT and MILTER_PORT to control where the daemons
should listen.

