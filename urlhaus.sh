#!/bin/sh

RELOAD=0
CLAMDIR=/var/lib/clamav

rm -fr /tmp/urlhaus
mkdir /tmp/urlhaus

curlopts="-sfL"
if [ -n "$DEBUG_URLHAUS" ]; then
  curlopts="$curlopts -v"
  set -x
fi

curl $curlopts https://urlhaus.abuse.ch/downloads/urlhaus.ndb -o /tmp/urlhaus/urlhaus.ndb

if [ $? -eq 0 ]; then
  clamscan --quiet -d /tmp/urlhaus /tmp/urlhaus >/dev/null 2>&1
  if [ $? -eq 0 ]; then
    if [ -f "$CLAMDIR"/urlhaus.ndb ]; then
      if ! cmp --quiet "$CLAMDIR"/urlhaus.ndb /tmp/urlhaus/urlhaus.ndb; then
        cp /tmp/urlhaus/urlhaus.ndb $CLAMDIR/
        RELOAD=1
      fi
    else
      cp /tmp/urlhaus/urlhaus.ndb $CLAMDIR/
      RELOAD=1
    fi
  fi
fi

if [ $RELOAD -eq 1 ]; then
  if [ -e /tmp/clamd.pid ]; then
    echo "urlhaus.ndb updated, sending SIGUSR2 to clamd"
    kill -USR2 `cat /tmp/clamd.pid`
  fi
fi

rm -fr /tmp/urlhaus

exit 0
