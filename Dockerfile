FROM registry.git.autistici.org/ai3/docker/s6-overlay-lite:master

COPY build.sh /tmp/build.sh
COPY urlhaus.sh /usr/local/bin/urlhaus.sh
RUN /tmp/build.sh && rm /tmp/build.sh
COPY conf/ /etc/

